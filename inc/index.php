<?php
/* scan folder */

include('inc/scanDir.class.php');
/* $files=scanDir::scan('.', 'jpg'); */
$files = preg_grep('~\.(jpeg|jpg|png)$~', scandir('.'));

/* html headers */
?>
<!DOCTYPE HTML>
<html>
    <head>
	<!-- title -->
	<title>Photos...</title>
	<!-- encoding -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<!-- icon -->
	<link rel="icon" type="image/png" href="inc/ico.png" />
	<!-- css -->
	<link rel="stylesheet" href="inc/css/style.css" />

	<!-- bootstrap js -->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<!-- bootstrap css -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<!-- bootstrap option -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	
	<!-- magnific popup js -->
	<link rel="stylesheet" href="inc/css/magnific-popup.css">
	<!-- magnific popup css -->
	<script src="inc/js/jquery.magnific-popup.js"></script>
    </head>

<body>
    <div class="container-fluid">
	<div class="row">
	    <div class="col-sm-1"></div>
	    <div class="col-sm-10">
		<a href=".."><div class="pplink"></div></a>
	    </div>
	    <div class="col-sm-1"></div>
	</div>
	<div class="row">	
	    <div class="col-sm-1"></div>
	    <div class="col-sm-10">
		<div class="thumbs">
		    <ul>
<?php 
    foreach($files as $pict_key=>$pict_name) {
        if(file_exists('./thumbs/'.$pict_name)) {
            echo '<li><a href="'.$pict_name.'" title="'.substr($pict_name, 2).'"><img src="./thumbs/'.$pict_name.'"></a></li>';
        }
    }
?>
		    </ul>
</div>
</div>
<div class="col-sm-1"></div>
	</div>
	<div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-10">
    <a href=".."><div class="pplink"></div></a>
    </div>
    <div class="col-sm-1"></div>
	</div>
    </div>
    </body>

    <script>
    $(document).ready(function() {
        $('.thumbs').magnificPopup({
          delegate: 'a',
                type: 'image',
                tLoading: 'Loading image #%curr%...',
                mainClass: 'mfp-img-mobile',
                gallery: {
              enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                    },
                image: {
              tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                    titleSrc: function(item) {
                    return '<a href="./'+item.el.attr('title')+'">'+item.el.attr('title')+'</a>';
                }
            }
        });
    });
</script>
</html>



import logging.config
from decouple import config
import os
import shutil

logging.config.fileConfig("./logging.conf")
log = logging.getLogger("gallery")

# get configuration variables
PIC_FOLDER = config("PIC_FOLDER", ".")
FILTERED_FOLDER = config("FILTERED_FOLDER", "").split(":")
PIC_EXTENSIONS = config("PIC_EXTENSIONS", "jpg:jpeg").split(":")
TMP_FOLDER = config("TMP_FOLDER", "tmp")
REMOTE_SERVER = config("REMOTE_SERVER")
REMOTE_FOLDER = config("REMOTE_FOLDER")

print(f'picture folder: {PIC_FOLDER}')

# loop over folders
for dir_name in os.listdir(PIC_FOLDER):

    # skip filtered folder
    if dir_name in FILTERED_FOLDER:
        log.debug(f'folder: {dir_name} (skipped)')
        continue

    dir_path = os.path.join(PIC_FOLDER, dir_name)

    log.info(f'folder: {dir_path}')

    # STEP 1: create local folder
    rdir_path = os.path.join(TMP_FOLDER, dir_name)
    try:
        os.makedirs(os.path.join(rdir_path, "thumbs"))
        log.debug(f'  create folder')
    except FileExistsError as e:
        log.debug(f'  folder already exists')

    # STEP 2: get all pictures files and reduce them in the new folder
    for f in os.listdir(dir_path):

        ext = f.split(".")[-1].lower()
        if ext not in PIC_EXTENSIONS:
            log.debug(f'  file ignored: {f}')
            continue

        log.debug(f'  picture: {f}')

        f_path = os.path.join(dir_path, f)

        # reduce/thumbnail picture
        steps = {
            "thumbnail": {
                "path": os.path.join(os.path.join(rdir_path,"thumbs"), f),
                "resize": "200x100"
            },
            "recude": {
                "path": os.path.join(rdir_path, f),
                "resize": "1200x1200"
            }
        }
        for k, v in steps.items():

            if not os.path.exists(v["path"]):
                log.info(f'  --> {k}: {f}')
                cmd = [
                    'convert',
                    '-auto-orient',
                    os.path.join(dir_path, f),
                    f'-resize {v["resize"]}',
                    v["path"]
                ]
                os.system(' '.join(cmd))

    # STEP 3: copy static files (index.php/inc) in each folder
    try:
        shutil.copytree("inc", os.path.join(rdir_path,"inc"))
        log.info("  copy folder inc")
    except FileExistsError as e:
        log.info("  folder inc already exists")

    index_src = os.path.join(rdir_path,"inc/index.php")
    index_dest = os.path.join(rdir_path,"index.php")
    if not os.path.exists(index_dest):
        log.debug(f'  copy index.php {index_src} -> {index_dest}')
        try:
            shutil.copy2(index_src, index_dest)
        except FileNotFoundError as e:
            log.warning(f'  copy index.php: symlink to delete')

# STEP 4: sync with remote
cmd = [
    'rsync',
    '-az',
    '--verbose',
    '--delete',
    f'{TMP_FOLDER}/*',
    f'{REMOTE_SERVER}:{REMOTE_FOLDER}'
]
log.info(' '.join(cmd))
os.system(' '.join(cmd))
